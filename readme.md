Star Wars Shockball
============
A star wars sports simulation game for the browser, built using the [Phaser](https://phaser.io/) game framework.

### Install
* `git clone git@gitlab.com:bpkennedy/star-wars-shockball.git`
* `cd star-wars-shockball && npm install`

### Develop
* `npm run dev` - webpack compiles `/src` files and serves on localhost:8080, with `webpack-dev-server` watching changes *to `/src` files only* and reloading on each change.
* `npm run lint` - runs eslint on `/src` and `/test` files.  This is a modified version (loosened) of the .eslint settings for the [Phaser project](https://github.com/photonstorm/phaser/blob/master/.eslintrc.json).
