import * as jest from 'jest';

export default class MockScene {
  constructor() {
    this.physics = {
      add: {
        sprite: jest.fn().mockReturnValue({
          setDrag: jest.fn().mockReturnValue({
            setMaxVelocity: jest.fn().mockReturnValue({
              setSize: jest.fn().mockReturnValue({
                setOffset: jest.fn().mockReturnValue({
                  destroy: jest.fn().mockReturnValue()
                })
              })
            })
          })
        })
      }
    };
  }

  setPhysics(physicsMock) {
    this.physics = physicsMock;
  }

  setSprite(spriteMock) {
    this.physics.add.sprite = spriteMock;
  }
}
