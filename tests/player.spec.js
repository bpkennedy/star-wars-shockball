import { test, expect } from 'jest';
import { MockScene } from './phaserMock';
import Player from '../src/objects/player.js';
const mockScene = new MockScene();

test('player sprite is destroyed when destroy is called', () => {
  const player = new Player(mockScene, 5, 5);
  player.destroy();
  expect(player.sprite.destroy).toBeCalledTimes(1);
});
