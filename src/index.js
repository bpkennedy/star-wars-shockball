import 'phaser';
import { SimpleScene } from './scenes/simple-scene';

const gameConfig = {
  width: 680,
  height: 400,
  scene: SimpleScene,
  physics: {
    default: 'arcade',
    arcade: {
      gravity: {
        y: 1000
      }
    }
  }
};

new Phaser.Game(gameConfig);
