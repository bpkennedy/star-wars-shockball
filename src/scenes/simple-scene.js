import Player from '../objects/player.js';

export class SimpleScene extends Phaser.Scene {
  preload() {
    this.load.image('cokecan', 'assets/cokecan.png');
  }

  create() {
    this.player = new Player(this, 5, 5);
    this.add.text(100, 100, 'Hello Phaser!', {
      fill: '#0f0'
    });
    this.add.image(100, 200, 'cokecan');
  }
}
