export default class Player {
  constructor(scene, x, y) {
    this.scene = scene;
    this.sprite = this.scene.physics.add
      .sprite(x, y, 'player', 0)
      .setDrag(1000, 0)
      .setMaxVelocity(300, 400)
      .setSize(18, 24)
      .setOffset(7, 9);
  }

  update() {

  }

  destroy() {
    this.sprite.destroy();
  }
}
